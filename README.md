# Articles of Confederation

*Articles of Confederation and Perpetual Union, between the States of*

- *New Hampshire,*
- *Massachusetts Bay,*
- *Rhode Island and Providence Plantations,*
- *Connecticut,*
- *New York,*
- *New Jersey,*
- *Pennsylvania,*
- *Delaware,*
- *Maryland,*
- *Virginia,*
- *North Carolina,*
- *South Carolina,*
- *Georgia.*

**Art. 1.** The stile of this Confederacy shall be "The United States of America."

**Art. 2.** Each State retains its sovereignty, freedom and independence, and every power, jurisdiction, and right, which is not by this confederation expressly delegated to the United States, in Congress assembled.

**Art. 3.** The said states hereby severally enter into a firm league of friendship with each other for their common defence, the security of their liberties and their mutual and general welfare; binding themselves to assist each other against all force offered to, or attacks made upon them, or any of them, on account of religion, sovereignty, trade, or any other pretence whatever.

**Art. 4.** The better to secure and perpetuate mutual friendship and intercourse among the people of the different states in this union, the free inhabitants of each of these states, paupers, vagabonds, and fugitives from justice excepted, shall be entitled to all privileges and immunities of free citizens in the several states; and the people of each State shall have free ingress and regress to and from any other State, and shall enjoy therein all the privileges of trade and commerce, subject to the same duties, impositions, and restrictions, as the inhabitants thereof respectively; provided, that such restrictions shall not extend so far as to prevent the removal of property, imported into any State, to any other State of which the owner is an inhabitant; provided also, that no imposition, duties, or restriction, shall be laid by any State on the property of the United States, or either of them.

If any person guilty of, or charged with treason, felony, or other high misdemeanor in any State, shall flee from justice and be found in any of the United States, he shall, upon demand of the governor or executive power of the State from which he fled, be delivered up and removed to the State having jurisdiction of his offence.

Full faith and credit shall be given in each of these states to the records, acts, and judicial proceedings of the courts and magistrates of every other State.

**Art. 5.** For the more convenient management of the general interests of the United States, delegates shall be annually appointed, in such manner as the legislature of each State shall direct, to meet in Congress, on the 1st Monday in November in every year, with a power reserved to each State to recal its delegates, or any of them, at any time within the year, and to send others in their stead for the remainder of the year.

No State shall be represented in Congress by less than two, nor by more than seven members; and no person shall be capable of being a delegate for more than three years in any term of six years; nor shall any person, being a delegate, be capable of holding any office under the United States, for which he, or any other for his benefit, receives any salary, fees, or emolument of any kind.

Each state shall maintain its own delegates in a meeting of the states and while they act as members of the committee of the states.

In determining questions in the united states in Congress assembled each state shall have one vote.

Freedom of speech and debate in Congress shall not be impeached or questioned in any court or place out of Congress: and the members of Congress shall be protected in their persons from arrests and imprisonments, during the time of their going to and from, and attendance on Congress, except for treason, felony, or breach of the peace.

**Art. 6.** No State, without the consent of the United States, in Congress assembled
1. shall send any embassy to, or receive any embassy from, or enter into any conference, agreement, alliance, or treaty with any king, prince, or state

2. nor shall any person, holding any office of profit or trust under the United States, or any of them, accept of any present, emolument, office or title, of any kind whatever, from any king, prince, or foreign state

3. nor shall the United States, in Congress assembled, or any of them, grant any title of nobility.

No two or more states shall enter into any treaty, confederation, or alliance, whatever, between them, without the consent of the United States, in Congress assembled, specifying accurately the purposes for which the same is to be entered into, and how long it shall continue.

No state shall lay any imposts or duties which may interfere with any stipulations in treaties entered into by the United States, in Congress assembled, with any king, prince, or state, in pursuance of any treaties already proposed by Congress to the courts of France and Spain.

No vessels of war shall be kept up in time of peace by any State, except such number only as shall be deemed necessary by the United States, in Congress assembled, for the defence of such State or its trade; nor shall any body of forces be kept up by any State, in time of peace, except such number only as, in the judgment of the United States, in Congress assembled, shall be deemed requisite to garrison the forts necessary for the defence of such State; but every State shall always keep up a well regulated and disciplined militia, sufficiently armed and accoutred, and shall provide, and constantly have ready for use, in public stores, a due number of field pieces and tents, and a proper quantity of arms, ammunition and camp equipage.

No State shall engage in any war without the consent of the United States, in Congress assembled, unless such State be actually invaded by enemies, or shall have received certain advice of a resolution being formed by some nation of Indians to invade such State, and the danger is so imminent as not to admit of a delay till the United States, in Congress assembled, can be consulted; nor shall any State grant commissions to any ships or vessels of war, nor letters of marque or reprisal, except it be after a declaration of war by the United States, in Congress assembled, and then only against the kingdom or state, and the subjects thereof, against which war has been so declared, and under such regulations as shall be established by the United States, in Congress assembled, unless such State be infested by pirates, in which case vessels of war may be fitted out for that occasion, and kept so long as the danger shall continue, or until the United States, in Congress assembled, shall determine otherwise.

**Art. 7.** When land forces are raised by any State for the common defence, all officers of or under the rank of colonel, shall be appointed by the legislature of each State respectively, by whom such forces shall be raised, or in such manner as such State shall direct; and all vacancies shall be filled up by the State which first made the appointment.

**Art. 8.** All charges of war and all other expences, that shall be incurred for the common defence or general welfare, and allowed by the United States, in Congress assembled, shall be defrayed out of a common treasury, which shall be supplied by the several states, in proportion to the value of all land within each State, granted to or surveyed for any person, as such land and the buildings and improvements thereon shall be estimated according to such mode as the United States, in Congress assembled, shall, from time to time, direct and appoint.

The taxes for paying that proportion shall be laid and levied by the authority and direction of the legislatures of the several states, within the time agreed upon by the United States, in Congress assembled.

**Art. 9.** The United States, in Congress assembled, shall have the sole and exclusive right and power of:

1. Determining on peace and war, except in the cases mentioned in the 6th article

2. Sending and receiving ambassadors

3. Entering into treaties and alliances, provided that no treaty of commerce shall be made, whereby the legislative power of the respective states shall be restrained from imposing such imposts and duties on foreigners as their own people are subjected to, or from prohibiting the exportation or importation of any species of goods or commodities whatsoever

4. Establishing rules for deciding, in all cases, what captures on land or water shall be legal, and in what manner prizes, taken by land or naval forces in the service of the United States, shall be divided or appropriated

5. Granting letters of marque and reprisal in times of peace

6. Appointing courts for the trial of piracies and felonies committed on the high seas, and establishing courts for receiving, and establishing courts for receiving and determining, finally, appeals in all cases of captures; provided, that no member of Congress shall be appointed a judge of any of the said courts.

The United States, in Congress assembled, shall also be the last resort on appeal in all disputes and differences now subsisting, or that hereafter may arise between two or more states concerning boundary, jurisdiction or any other cause whatever; which authority shall always be exercised in the manner following:

1. whenever the legislative or executive authority, or lawful agent of any State, in controversy with another, shall present a petition to Congress, stating the matter in question, and praying for a hearing, notice thereof shall be given, by order of Congress, to the legislative or executive authority of the other State in controversy, and a day assigned for the appearance of the parties by their lawful agents, who shall then be directed to appoint, by joint consent, commissioners or judges to constitute a court for hearing and determining the matter in question;

2. but, if they cannot agree, Congress shall name three persons out of each of the United States, and from the list of such persons each party shall alternately strike out one, the petitioners beginning, until the number shall be reduced to thirteen;

3. and from that number not less than seven, nor more than nine names, as Congress shall direct, shall, in the presence of Congress, be drawn out by lot;

4. and the persons whose names shall be so drawn, or any five of them, shall be commissioners or judges to hear and finally determine the controversy, so always as a major part of the judges who shall hear the cause shall agree in the determination;

5. and if either party shall neglect to attend at the day appointed, without shewing reasons which Congress shall judge sufficient, or, being present, shall refuse to strike, the Congress shall proceed to nominate three persons out of each State, and the secretary of Congress shall strike in behalf of such party absent or refusing;

6. and the judgment and sentence of the court to be appointed, in the manner before prescribed, shall be final and conclusive;

7. and if any of the parties shall refuse to submit to the authority of such court, or to appear or defend their claim or cause, the court shall nevertheless proceed to pronounce sentence or judgment, which shall, in like manner, be final and decisive, the judgment or sentence and other proceedings being, in either case, transmitted to Congress, and lodged among the acts of Congress for the security of the parties concerned;

8. provided, that every commissioner, before he sits in judgment, shall take an oath, to be administered by one of the judges of the supreme or superior court of the State where the cause shall be tried, "well and truly to hear and determine the matter in question, according to the best of his judgment, without favour, affection, or hope of reward:" provided, also; that no State shall be deprived of territory for the benefit of the United States.

The United States, in Congress assembled, shall also have the sole and exclusive right and power of

1. Regulating the alloy and value of coin struck by their own authority, or by that of the respective states

2. Fixing the standard of weights and measures throughout the United States

3. Regulating the trade and managing all affairs with the Indians not members of any of the states; provided that the legislative right of any State within its own limits be not infringed or violated

4. Establishing and regulating post offices from one State to another throughout all the United States, and exacting such postage on the papers passing through the same as may be requisite to defray the expences of the said office

5. Appointing all officers of the land forces in the service of the United States, excepting regimental officers

6. Appointing all the officers of the naval forces, and commissioning all officers whatever in the service of the United States

7. Making rules for the government and regulation of the said land and naval forces, and directing their operations.

The United States, in Congress assembled, shall have authority to appoint a committee to sit in the recess of Congress, to be denominated "a Committee of the States," and to consist of one delegate from each State, and to

1. Appoint such other committees and civil officers as may be necessary for managing the general affairs of the United States, under their direction
2. Appoint one of their number to preside; provided that no person be allowed to serve in the office of president more than one year in any term of three years
3. Ascertain the necessary sums of money to be raised for the service of the United States, and to appropriate and apply the same for defraying the public expences
4. Borrow money or emit bills on the credit of the United States, transmitting, every half year, to the respective states, an account of the sums of money so borrowed or emitted
5. Build and equip a navy
6. Agree upon the number of land forces, and to make requisitions from each State for its quota, in proportion to the number of white inhabitants in such State; which requisitions shall be binding; and, thereupon, the legislature of each State shall appoint the regimental officers, raise the men, and cloathe, arm, and equip them in a soldier-like manner, at the expence of the United States; and the officers and men so cloathed, armed, and equipped, shall march to the place appointed and within the time agreed on by the United States, in Congress assembled;

    but if the United States, in Congress assembled, shall, on consideration of circumstances, judge proper that any State should not raise men, or should raise a smaller number than its quota, and that any other State should raise a greater number of men than the quota thereof, such extra number shall be raised, officered, cloathead, armed, and equipped in the same manner as the quota of such State, unless the legislature of such State shall judge that such extra number cannot be safely spared out of the same, in which case they shall raise, officer, cloathe, arm, and equip as many of such extra number as they judge can be safely spared. And the officers and men so cloathed, armed, and equipped, shall march to the place appointed and within the time agreed on by the United States, in Congress assembled.

The United States, in Congress assembled, shall never
1. engage in a war, nor
2. grant letters of marque and reprisal in time of peace, nor
3. enter into any treaties or alliances, nor
4. coin money, nor regulate the value thereof, nor
5. ascertain the sums and expences necessary for the defence and welfare of the United States, or any of them: nor
6. emit bills, nor
7. borrow money on the credit of the United States, nor
8. appropriate money, nor
9. agree upon the number of vessels of war to be built or purchased, or the number of land or sea forces to be raised, nor
10. appoint a commander in chief of the army or navy, unless nine states assent to the same

Nor shall a question on any other point, except for adjourning from day to day, be determined, unless by the votes of a majority of the United States, in Congress assembled.

No person holding any office under the United States, for which he, or another for his benefit, receives any salary, fees, or emolument of any kind, shall be capable of being a Delegate.

The Congress of the United States shall have power to adjourn to any time within the year, and to any place within the United States, so that no period of adjournment be for a longer duration than the space of six months, and shall publish the journal of their proceedings monthly, except such parts thereof, relating to treaties, alliances or military operations, as, in their judgment, require secrecy; and the yeas and nays of the delegates of each State on any question shall be entered on the journal, when it is desired by any delegate; and the delegates of a State, or any of them, at his, or their request, shall be furnished with a transcript of the said journal, except such parts as are above excepted, to lay before the legislatures of the several states.

All controversies concerning the private right of soil, claimed under different grants of two or more states, whose jurisdictions, as they may respect such lands and the states which passed such grants, are adjusted, the said grants, or either of them, being at the same time claimed to have originated antecedent to such settlement of jurisdiction, shall, on the petition of either party to the Congress of the United States, be finally determined, as near as may be, in the same manner as is before prescribed for deciding disputes respecting territorial jurisdiction between different states.

**Art. 10.** The committee of the states, or any nine of them, shall be authorized to execute, in the recess of Congress, such of the powers of Congress as the United States, in Congress assembled, by the consent of nine states, shall, from time to time, think expedient to vest them with; provided, that no power be delegated to the said committee, for the exercise of which, by the articles of confederation, the voice of nine states, in the Congress of the United States assembled, is requisite.

**Art. 11.** Canada acceding to this confederation, and joining in the measures of the United States, shall be admitted into and entitled to all the advantages of this union; but no other colony shall be admitted into the same, unless such admission be agreed to by nine states.

**Art. 12.** All bills of credit emitted, monies borrowed and debts contracted by, or under the authority of Congress before the assembling of the United States, in pursuance of the present confederation, shall be deemed and considered as a charge against the United States, for payment and satisfaction whereof the said United States and the public faith are hereby solemnly pledged.

**Art. 13.** Every State shall abide by the determinations of the United States, in Congress assembled, on all questions which, by this confederation, are submitted to them. And the articles of this confederation shall be inviolably observed by every State, and the union shall be perpetual; nor shall any alteration at any time hereafter be made in any of them, unless such alteration be agreed to in a Congress of the United States, and be afterwards confirmed by the legislatures of every State.

These articles shall be proposed to the legislatures of all the United States, to be considered, and if approved of by them, they are advised to authorize their delegates to ratify the same in the Congress of the United States; which being done, the same shall become conclusive.
